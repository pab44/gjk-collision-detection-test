#include "gjk.h"
#include <cstdio>

void unit_test_1_no_collisision()
{
    //determines the collision of two convex shapes in 3d
    

    // side of a tetrahedron defined by three (end) points: 
    //P1(2.x, t2.y, t2.z), P2(t1.x, t1.y, t1.z), P3(t3.x, t3.y, t3.z)
    float p1_x = 10; float p1_y = 10; float p1_z = 10;
    float p2_x = 12; float p2_y = 12; float p2_z = 12;
    float p3_x = 12; float p3_y = 10; float p3_z = 10;
    shape shape1[] = {p1_x, p1_y, p1_z, p2_x, p2_y, p2_z, p3_x, p3_y, p3_z};

    // side of a cube defined by four (end) points: 
    //P1(size, size, size), P2(size, size, -size), P3(size, -size, -size), P4(size, -size, size)
    float size = 2;
    shape shape2[] = {size, size, size,  size, size, -size,  size, -size, -size,  size, -size, size }; 


     // (amount of points P_i of the shape 1) -> here: 3
    int dim_ts = sizeof(shape1) / sizeof(*shape1);

    // (amount of points P_i of the shape 2) -> here: 4
    int dim_shape2 = sizeof(shape2) / sizeof(*shape2); 

    //tests to see if 1 side of a shapes collides with another side of a face 
    bool check_collision = gjk(&shape1[0], &shape2[0], dim_ts, dim_shape2); // 0 ... no collision, 1 ... collision

    if(check_collision){printf("Failed unit test 1.\n");}
    else{printf("Passed unit test 1.\n");}
}

void unit_test_2_collisision()
{
    //determines the collision of two convex shapes in 3d
    

    // side of a tetrahedron defined by three (end) points: 
    //P1(2.x, t2.y, t2.z), P2(t1.x, t1.y, t1.z), P3(t3.x, t3.y, t3.z)
    float p1_x = 0; float p1_y = 0; float p1_z = 0;
    float p2_x = 2; float p2_y = 2; float p2_z = 2;
    float p3_x = 2; float p3_y = 0; float p3_z = 0;
    shape shape1[] = {p1_x, p1_y, p1_z, p2_x, p2_y, p2_z, p3_x, p3_y, p3_z};

    // side of a cube defined by four (end) points: 
    //P1(size, size, size), P2(size, size, -size), P3(size, -size, -size), P4(size, -size, size)
    float size = 2;
    shape shape2[] = {size, size, size,  size, size, -size,  size, -size, -size,  size, -size, size }; 


     // (amount of points P_i of the shape 1) -> here: 3
    int dim_ts = sizeof(shape1) / sizeof(*shape1);

    // (amount of points P_i of the shape 2) -> here: 4
    int dim_shape2 = sizeof(shape2) / sizeof(*shape2); 

    //tests to see if 1 side of a shapes collides with another side of a face 
    bool check_collision = gjk(&shape1[0], &shape2[0], dim_ts, dim_shape2); // 0 ... no collision, 1 ... collision

    if(!check_collision){printf("Failed unit test 2.\n");}
    else{printf("Passed unit test 2.\n");}
}

int main(int argc, char* argv[])
{
    unit_test_1_no_collisision();
    unit_test_2_collisision();

    return 0;
}