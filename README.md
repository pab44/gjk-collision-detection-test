# Gilbert–Johnson–Keerthi (GJK) algorithm

## Note: fork of GJK-collision-detection by clauskovacs
https://github.com/clauskovacs/GJK-collision-detection-3d/blob/master/gjk.h

Determines the collision of two convex polygons in three dimensions at any position relative to each other.

### More information
Detailed description of the algorithm including operational videos available at

http://www.phys.ik.cx/programming/cpp/collision/01/index.php
